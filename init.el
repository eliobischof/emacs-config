
;; Emacs init functions
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.


;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

(defconst codepath (getenv "GOPATH") "Matches also the GOPATH")
(defconst initfile (getenv "DEFAULT_FILE"))

(require 'package)
(add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/") t)

(defvar my-packages '(better-defaults paredit idle-highlight-mode ido-ubiquitous
                      find-file-in-project smex flycheck auto-complete
		              go-mode go-eldoc go-direx exec-path-from-shell protobuf-mode
                      haskell-mode intero rainbow-delimiters markdown-mode pdf-tools
                      dockerfile-mode magit docker-compose-mode))
(package-initialize)
(package-refresh-contents)
(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

()
(load-theme 'wheatgrass)

(defun set-exec-path-from-shell-PATH ()
  (let ((path-from-shell (replace-regexp-in-string "[ \t\n]*$" "" (shell-command-to-string "$SHELL --login -i -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq eshell-path-env path-from-shell) ; for eshell users
    (setq exec-path (split-string path-from-shell path-separator))))

(when window-system (set-exec-path-from-shell-PATH))

;(defun compile-on-save-start ()
;  (let ((buffer (compilation-find-buffer)))
;    (unless (get-buffer-process buffer) 
;      (recompile))))
(defun compile-on-save-start ()
  (recompile)
  (delete-other-windows))

(define-minor-mode compile-on-save-mode
  "Minor mode to automatically call `recompile' whenever the
current buffer is saved. When there is ongoing compilation,
nothing happens."
  :lighter " CoS"
    (if compile-on-save-mode
    (progn  (make-local-variable 'after-save-hook)
        (add-hook 'after-save-hook 'compile-on-save-start nil t))
      (kill-local-variable 'after-save-hook)))

(setenv "ENV" "LOCAL")
(setenv "codepath" codepath)
(add-to-list 'exec-path (concat codepath "/bin"))

;(add-to-list 'load-path "/home/ubuntu/git/src/github.com/ptrv/goflycheck")
;(require 'go-flycheck)

(add-to-list 'load-path (concat codepath "/src/github.com/nsf/gocode/emacs/"))
(require 'auto-complete-config)
(require 'go-autocomplete)

;(global-flycheck-mode)

(ac-config-default)

(defun my-prog-mode-hook()
  (subword-mode)
  (linum-mode)

)
(add-hook 'prog-mode-hook 'my-prog-mode-hook)
(add-hook 'emacs-lisp-mode-hook #'rainbow-delimiters-mode)

(defun my-go-mode-hook ()
    ; Use goimports instead of go-fmt
;    (setq gofmt-command "goimports")
    ; Call Gofmt before saving

    (add-hook 'before-save-hook 'gofmt-before-save)

    ; Customize compile command to run go build
    (if (not (string-match "go" compile-command))
      (set (make-local-variable 'compile-command)
        "go build -v && go test -v && go vet"))
    ; Godef jump key binding
    (local-set-key (kbd "M-.") 'godef-jump)
    (local-set-key (kbd "M-*") 'pop-tag-mark)
    (local-set-key (kbd "C-x C-j") 'godef-jump-other-window)
    (auto-complete-mode 1)
    (go-eldoc-setup)
    (flycheck-mode)
;    (local-set-key (kbd "C-c C-j") 'go-direx-pop-to-buffer)
    (compile-on-save-mode)
)
(add-hook 'go-mode-hook 'my-go-mode-hook)

(defun my-haskell-mode-hook()
  (custom-set-variables
   '(haskell-stylish-on-save t)
   '(haskell-tags-on-save t)
   '(haskell-process-suggest-remove-import-lines t)
   '(haskell-process-auto-import-loaded-modules t)
   '(haskell-process-log t))
  (intero-mode)
  (flycheck-mode)
  (flycheck-add-next-checker 'intero '(warning . haskell-hlint))
  (rainbow-delimiters-mode-enable)
)
(add-hook 'haskell-mode-hook 'my-haskell-mode-hook)

(defun my-markdown-mode-hook()
  (markdown-live-preview-mode)
;  (add-hook 'post-self-insert-hook 'save-buffer)
  )
(add-hook 'markdown-mode-hook 'my-markdown-mode-hook)

(pdf-tools-install)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-suggest-remove-import-lines t)
 '(haskell-stylish-on-save t)
 '(haskell-tags-on-save t)
 '(inhibit-startup-screen t)
 '(package-selected-packages
   (quote
    (docker-compose-mode exec-path-from-shell smex paredit neotree magit ido-ubiquitous idle-highlight-mode go-mode go-autocomplete flycheck find-file-in-project better-defaults))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(global-set-key (kbd "C-x g") 'magit-status)
(electric-pair-mode)
(setq-default tab-width 4)

(find-file initfile)


